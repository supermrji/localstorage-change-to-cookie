initStorage : function () {
    var storageImpl;

    try {
        window.localStorage.setItem("storage", "");
        window.localStorage.removeItem("storage");
        storageImpl = window.localStorage;
    } catch (err) {

        function LocalStorageAlternative() {

            var structureLocalStorage = {};
            this.setItem = function (sKey, sValue) {
                if (!sKey) {
                    return;
                }
                document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
                this.length = document.cookie.match(/\=/g).length;
            }

            this.getItem = function (sKey) {
                if (!sKey || !this.hasOwnProperty(sKey)) {
                    return null;
                }
                return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
            }

            this.removeItem = function (sKey) {
                if (!sKey || !this.hasOwnProperty(sKey)) {
                    return;
                }
                document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
            },
            this.hasOwnProperty =  function (sKey) {
                return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
            }
        }

        storageImpl = new LocalStorageAlternative();
    }

    return storageImpl;

},
setStorage: function (key, value) {
    value = JSON.stringify({
        'k': value
    });
    _self.localStorage.setItem(key, value);
},
getStorage: function (key) {
    var value = _self.localStorage.getItem(key);
    value = JSON.parse(value);
    return (value == null) ? value : value.k;
},
delStorage: function (key) {
    _self.localStorage.removeItem(key);
}